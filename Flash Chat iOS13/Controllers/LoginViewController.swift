//
//  LoginViewController.swift
//  Flash Chat iOS13
//
//  Created by Angela Yu on 21/10/2019.
//  Copyright © 2019 Angela Yu. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    

    @IBAction func loginPressed(_ sender: UIButton) {
        
        var getEmail = emailTextfield.text!
        var getContraseña = passwordTextfield.text!
        
        if getEmail != "seba" {
            
            emailTextfield.text = ""
            emailTextfield.placeholder = "usuario no encontrado"
            

        }
        else if getContraseña != "7902" {
            
            passwordTextfield.text = ""
            passwordTextfield.placeholder = "contraseña incorrecta"
        }
        
        else {
            performSegue(withIdentifier: "login", sender: self)
        }
    }
    
}
